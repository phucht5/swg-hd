import { en } from './lang/en_EN';
import { vn } from './lang/vi_VN';
import { ILang } from './lang/interface';

const langs: { [key: string]: ILang } = {
  vi: vn,
  en,
};

export { langs };
