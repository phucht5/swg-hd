import { useRouter } from 'next/router';

import { langs } from 'locale';

const useTrans = () => {
  const { locale } = useRouter();

  return locale && langs[locale] ? langs[locale] : langs.en;
};
export default useTrans;
