import { useRouter } from 'next/router';

const URL_EXAMPLE =
  'https://raw.githubusercontent.com/OAI/OpenAPI-Specification/main/examples/v2.0/yaml/uber.yaml';

const useParams = () => {
  const { query } = useRouter();
  const s =
    !query.s || Array.isArray(query.s) ? URL_EXAMPLE : String(query.s || '');

  const params = {
    sourceSwaggerUrl: s,
  };
  return params;
};
export default useParams;
