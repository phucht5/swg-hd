import { StatusType } from 'redux/interface';
import { LOGIN } from 'redux/constant';

export interface IUser {
  mail: string;
  name: string;
  role: string;
  avatar: string;
  token: string;
}

export interface IAuth {
  status: StatusType;
  user: IUser | null;
  error: string | null;
}
// action request
export interface FetchLoginSuccessPayload {
  user: IUser;
}

export interface FetchLoginRequestPayload {
  email: string;
  password: string;
}
export interface FetchLoginFailurePayload {
  error: string;
}

// action response
export interface LoadAuthRequest {
  type: typeof LOGIN.PROCESSING;
  payload: null;
}

export type LoadAuthSuccess = {
  type: typeof LOGIN.SUCCESS;
  payload: FetchLoginSuccessPayload;
};

export type LoadAuthFailure = {
  type: typeof LOGIN.ERROR;
  payload: FetchLoginFailurePayload;
};

// action type
export type AuthActions = LoadAuthRequest | LoadAuthSuccess | LoadAuthFailure;
