import { HYDRATE } from 'next-redux-wrapper';

import { IAuth, AuthActions } from 'redux/type/authI';
import { status } from 'redux/interface';
import { LOGIN } from 'redux/constant';

const init: IAuth = {
  error: '',
  status: status.BEGIN,
  user: null,
};

export default (state = init, action: AuthActions) => {
  const payload = action?.payload || {};
  const type = action?.type || undefined;
  if (!type) {
    return state;
  }
  switch (action.type) {
    case HYDRATE:
      // Attention! This will overwrite client state! Real apps should use proper reconciliation.
      return { ...state, ...payload };
    case LOGIN.PROCESSING:
      return { ...state, status: status.PROCESSING };
    case LOGIN.SUCCESS:
      return { ...state, ...payload, status: status.SUCCESS };
    case LOGIN.ERROR:
      return { ...state, ...payload, status: state.error };
    default:
      return { ...state };
  }
};
