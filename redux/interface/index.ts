export enum status {
  BEGIN = 'begin',
  PROCESSING = 'processing',
  SUCCESS = 'success',
  ERROR = 'error',
}

export type StatusType =
  | status.BEGIN
  | status.PROCESSING
  | status.SUCCESS
  | status.ERROR;
