import { LOGIN } from 'redux/constant';
import {
  FetchLoginFailurePayload,
  FetchLoginSuccessPayload,
  LoadAuthRequest,
  LoadAuthSuccess,
  LoadAuthFailure,
} from 'redux/type/authI';

export const loadAuthRequest = (): LoadAuthRequest => ({
  type: LOGIN.PROCESSING,
  payload: null,
});

export const loadAuthSuccess = (
  payload: FetchLoginSuccessPayload,
): LoadAuthSuccess => ({
  type: LOGIN.SUCCESS,
  payload,
});

export const loadAuthFailure = (
  payload: FetchLoginFailurePayload,
): LoadAuthFailure => ({
  type: LOGIN.ERROR,
  payload,
});
