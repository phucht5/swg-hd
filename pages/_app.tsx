import '../styles/globals.css';
import type { AppProps } from 'next/app';
import { Provider } from 'react-redux';

import { wrapper } from 'redux/stores';

const MyApp = ({ Component, ...rest }: AppProps) => {
  const { props, store } = wrapper.useWrappedStore(rest);
  return (
    <Provider store={store}>
      <Component {...props} />
    </Provider>
  );
};
export async function getServerSideProps() {
  return {
    props: {}, // will be passed to the page component as props
  };
}
export default MyApp;
