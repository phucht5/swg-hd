/**
 * @type {import('next').NextConfig}
 **/
const nextConfig = {
  // reactStrictMode: true,
  swcMinify: true,
  output: 'standalone',
  i18n: {
    // providing the locales supported by your application
   locales: ["en", "vi"],
   //  default locale used when the non-locale paths are visited
   defaultLocale: "en",
 },
};

export default nextConfig;
