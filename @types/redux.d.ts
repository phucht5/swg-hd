/* eslint-disable @typescript-eslint/no-explicit-any */
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import * as Redux from 'redux';
import { Task } from 'redux-saga';

declare module 'redux' {
  export interface Store {
    sagaTask: Task; // provide the types for `store.sagaTask` here
  }
}
