import React from 'react';
import dynamic from 'next/dynamic';

import 'swagger-ui-react/swagger-ui.css';
import useParams from 'hook/useParams';

const SwaggerUI = dynamic(import('swagger-ui-react'), { ssr: false });
const SwaggerUIComponent = () => {
  const { sourceSwaggerUrl } = useParams();

  return (
    <div className="swagger-ui-div">
      <SwaggerUI url={sourceSwaggerUrl} />
    </div>
  );
};
export default SwaggerUIComponent;
